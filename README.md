# Blizzard API Client
PHP library for easier work with Blizzard's API.

**This package is under heavy development and may be a subject to significant changes!**

## Requirements
- PHP 7.1+

## Installation

`composer require danaketh/blizzard-client`


## Usage

You will need a developer account, which will allow you to generate API key and secret, which are required for use
of the client. You can [https://dev.battle.net/member/register](create and account)
on [https://dev.battle.net/](Battle.net Developer Portal).

When you get your key and secret, make sure that you **NEVER** include them in a public place where it can be obtained
by anyone (like project repository or website public directory).

```php
use danaketh\Blizzard\Exception\InvalidRegionException;
use danaketh\Blizzard\Game\WoW\API;
use danaketh\Blizzard\Game\WoW\Common\Region;


$api = new API('<API_KEY>', '<API_SECRET>');

// Set region
try {
    $api->setRegion(Region::Europe);
} catch (InvalidRegionException $e) {
    die($e->getMessage());
}

// Now get the realms endpoint
$realms = $api->realm();

foreach ($realms->findAll() as $r) {
    var_dump($r);
}
```
