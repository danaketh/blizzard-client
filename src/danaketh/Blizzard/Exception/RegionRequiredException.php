<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Exception;

/**
 * Class RegionRequiredException
 *
 * @package danaketh\Blizzard\Exception
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class RegionRequiredException extends \Exception
{

}
