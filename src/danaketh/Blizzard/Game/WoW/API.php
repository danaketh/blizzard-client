<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW;

use danaketh\Blizzard\Common\Locale;
use danaketh\Blizzard\Exception\InvalidRegionException;
use danaketh\Blizzard\Exception\RegionRequiredException;
use danaketh\Blizzard\Game\WoW\Common\Region;
use danaketh\Blizzard\Game\WoW\Endpoint\DataEndpoint;
use danaketh\Blizzard\Game\WoW\Endpoint\GuildEndpoint;
use danaketh\Blizzard\Game\WoW\Endpoint\RealmEndpoint;
use danaketh\Support\Mapper;



/**
 * Main API class for World of Warcraft
 *
 * @package danaketh\Blizzard\Game\WoW
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class API
{
    /**
     * @var string $apiKey API key
     */
    protected $apiKey;

    /**
     * @var string $apiSecret API secret
     */
    protected $apiSecret;

    /**
     * @var string $region Selected region
     */
    protected $region;

    /**
     * @var string $locale Selected locale for response
     */
    protected $locale = Locale::English;

    /**
     * @var Mapper $mapper Mapper class
     */
    protected $mapper;

    /**
     * @var \danaketh\Blizzard\Game\WoW\Endpoint\RealmEndpoint $realmEndpoint RealmEndpoint endpoint object
     */
    protected $realmEndpoint;

    protected $characterEndpoint;

    protected $guildProfileEndpoint;

    /**
     * @var DataEndpoint $dataEndpoint
     */
    protected $dataEndpoint;




    /**
     * API constructor.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct($key, $secret)
    {
        $this->apiKey = $key;
        $this->apiSecret = $secret;
    }




    /**
     * Select region from which the API gets data
     *
     * @param string $region
     *
     * @throws InvalidRegionException
     */
    public function setRegion($region): void
    {
        $regions = [
            Region::China,
            Region::Europe,
            Region::Korea,
            Region::SouthEastAsia,
            Region::Taiwan,
            Region::USA
        ];

        if (!\in_array($region, $regions, true)) {
            throw new InvalidRegionException(sprintf('Unknown region %s', $region));
        }

        $this->region = $region;
    }




    /**
     * @param string $locale
     */
    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }




    /**
     * @param Mapper $mapper
     */
    public function attachMapper(Mapper $mapper): void
    {
        $this->mapper = $mapper;
    }




    /**
     * @return RealmEndpoint
     * @throws InvalidRegionException
     * @throws RegionRequiredException
     */
    public function realm(): RealmEndpoint
    {
        if (!\is_string($this->region)) {
            throw new RegionRequiredException('Region has to be set!');
        }

        if (!$this->realmEndpoint) {
            $this->realmEndpoint = new RealmEndpoint($this->apiKey, $this->apiSecret);
            $this->realmEndpoint->setRegion($this->region);

            if ($this->mapper instanceof Mapper) {
                $this->realmEndpoint->attachMapper($this->mapper);
            }
        }

        return $this->realmEndpoint;
    }




    /**
     * @return GuildEndpoint
     * @throws InvalidRegionException
     * @throws RegionRequiredException
     */
    public function guild(): GuildEndpoint
    {
        if (!\is_string($this->region)) {
            throw new RegionRequiredException('Region has to be set!');
        }

        if (!$this->guildProfileEndpoint) {
            $this->guildProfileEndpoint = new GuildEndpoint($this->apiKey, $this->apiSecret);
            $this->guildProfileEndpoint->setRegion($this->region);

            if ($this->mapper instanceof Mapper) {
                $this->guildProfileEndpoint->attachMapper($this->mapper);
            }
        }

        return $this->guildProfileEndpoint;
    }




    /**
     * @return DataEndpoint
     * @throws InvalidRegionException
     * @throws RegionRequiredException
     */
    public function data(): DataEndpoint
    {
        if (!\is_string($this->region)) {
            throw new RegionRequiredException('Region has to be set!');
        }

        if (!$this->dataEndpoint) {
            $this->dataEndpoint = new DataEndpoint($this->apiKey, $this->apiSecret);
            $this->dataEndpoint->setRegion($this->region);

            if ($this->mapper instanceof Mapper) {
                $this->dataEndpoint->attachMapper($this->mapper);
            }
        }

        return $this->dataEndpoint;
    }
}
