<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Common;

class Faction
{
    public const ALLIANCE = 0;
    public const HORDE = 1;
    public const NEUTRAL = 2;
}
