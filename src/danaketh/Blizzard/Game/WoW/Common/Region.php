<?php
/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Common;


/**
 * Class Region
 *
 * @package danaketh\Blizzard\Game\WoW\Common
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class Region
{
    const Europe = 'eu'; // Europe
    const Taiwan = 'tw'; // Taiwan
    const USA = 'us'; // USA
    const China = 'cn'; // China
    const Korea = 'kr'; // Korea
    const SouthEastAsia = 'sea'; // South East Asia
}
