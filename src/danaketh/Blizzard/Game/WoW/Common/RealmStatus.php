<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Common;

class RealmStatus
{
    public const UNKNOWN = -1;
    public const IDLE = 0;
    public const POPULATING = 1;
    public const ACTIVE = 2;
    public const CONCLUDED = 3;
}
