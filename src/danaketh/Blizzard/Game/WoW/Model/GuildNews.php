<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Model;

/**
 * Class GuildNews
 *
 * @package danaketh\Blizzard\Game\WoW\Model
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class GuildNews
{
    protected $type;

    protected $character;

    protected $timestamp;

    protected $context;

    protected $bonusLists;

    protected $achievement;




    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }




    /**
     * @param mixed $type
     *
     * @return GuildNews
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getCharacter()
    {
        return $this->character;
    }




    /**
     * @param mixed $character
     *
     * @return GuildNews
     */
    public function setCharacter($character)
    {
        $this->character = $character;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }




    /**
     * @param mixed $timestamp
     *
     * @return GuildNews
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }




    /**
     * @param mixed $context
     *
     * @return GuildNews
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getBonusLists()
    {
        return $this->bonusLists;
    }




    /**
     * @param mixed $bonusLists
     *
     * @return GuildNews
     */
    public function setBonusLists($bonusLists)
    {
        $this->bonusLists = $bonusLists;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getAchievement()
    {
        return $this->achievement;
    }




    /**
     * @param mixed $achievement
     *
     * @return GuildNews
     */
    public function setAchievement($achievement)
    {
        $this->achievement = $achievement;

        return $this;
    }


}
