<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Model;


/**
 * Class Guild
 *
 * @package danaketh\Blizzard\Game\WoW\Model
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class Guild
{
    public const MEMBERS = 'members';

    public const ACHIEVEMENTS = 'achievements';

    public const NEWS = 'news';

    public const CHALLENGE = 'challenge';

    protected $name;

    protected $realm;

    protected $battleGroup;

    protected $level;

    protected $side;

    protected $achievementPoints;

    protected $lastModified;

    protected $emblem;

    protected $members;

    protected $achievements;

    protected $news;

    protected $challenge;




    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }




    /**
     * @param mixed $name
     *
     * @return Guild
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getRealm()
    {
        return $this->realm;
    }




    /**
     * @param mixed $realm
     *
     * @return Guild
     */
    public function setRealm($realm)
    {
        $this->realm = $realm;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getBattleGroup()
    {
        return $this->battleGroup;
    }




    /**
     * @param mixed $battleGroup
     *
     * @return Guild
     */
    public function setBattleGroup($battleGroup)
    {
        $this->battleGroup = $battleGroup;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }




    /**
     * @param mixed $level
     *
     * @return Guild
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getSide()
    {
        return $this->side;
    }




    /**
     * @param mixed $side
     *
     * @return Guild
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getAchievementPoints()
    {
        return $this->achievementPoints;
    }




    /**
     * @param mixed $achievementPoints
     *
     * @return Guild
     */
    public function setAchievementPoints($achievementPoints)
    {
        $this->achievementPoints = $achievementPoints;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }




    /**
     * @param mixed $lastModified
     *
     * @return Guild
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getEmblem()
    {
        return $this->emblem;
    }




    /**
     * @param mixed $emblem
     *
     * @return Guild
     */
    public function setEmblem($emblem)
    {
        $this->emblem = $emblem;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getMembers()
    {
        return $this->members;
    }




    /**
     * @param mixed $members
     *
     * @return Guild
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getAchievements()
    {
        return $this->achievements;
    }




    /**
     * @param mixed $achievements
     *
     * @return Guild
     */
    public function setAchievements(array $achievements): Guild
    {
        $this->achievements = $achievements;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }




    /**
     * @param mixed $news
     *
     * @return Guild
     */
    public function setNews(array $news)
    {
        $this->news = $news;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getChallenge()
    {
        return $this->challenge;
    }




    /**
     * @param mixed $challenge
     *
     * @return Guild
     */
    public function setChallenge($challenge)
    {
        $this->challenge = $challenge;

        return $this;
    }


}
