<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Model;

/**
 * Class RealmEndpoint
 *
 * @package danaketh\Blizzard\Game\WoW\Model
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class Realm
{
    protected $type;

    protected $population;

    protected $queue;

    protected $status;

    protected $name;

    protected $slug;

    protected $battleGroup;

    protected $locale;

    protected $timezone;

    protected $connectedRealms;




    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }




    /**
     * @param mixed $type
     *
     * @return Realm
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getPopulation()
    {
        return $this->population;
    }




    /**
     * @param mixed $population
     *
     * @return Realm
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getQueue()
    {
        return $this->queue;
    }




    /**
     * @param mixed $queue
     *
     * @return Realm
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }




    /**
     * @param mixed $status
     *
     * @return Realm
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }




    /**
     * @param mixed $name
     *
     * @return Realm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }




    /**
     * @param mixed $slug
     *
     * @return Realm
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getBattleGroup()
    {
        return $this->battleGroup;
    }




    /**
     * @param mixed $battleGroup
     *
     * @return Realm
     */
    public function setBattleGroup($battleGroup)
    {
        $this->battleGroup = $battleGroup;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }




    /**
     * @param mixed $locale
     *
     * @return Realm
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }




    /**
     * @param mixed $timezone
     *
     * @return Realm
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getConnectedRealms()
    {
        return $this->connectedRealms;
    }




    /**
     * @param mixed $connectedRealms
     *
     * @return Realm
     */
    public function setConnectedRealms($connectedRealms)
    {
        $this->connectedRealms = $connectedRealms;

        return $this;
    }
}
