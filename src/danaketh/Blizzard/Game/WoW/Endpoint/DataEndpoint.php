<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Endpoint;

use danaketh\Blizzard\Game\WoW\Model\Achievement;
use danaketh\Exception\RequestException;



class DataEndpoint extends AbstractEndpoint
{
    /**
     * @var string $endpoint
     */
    protected $endpoint = '/wow/data';




    /**
     * Get all character achievements
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     * @throws RequestException
     */
    public function getCharacterAchievements(): array
    {
        $data = $this->get('achievements', 'character/achievements');

        return $this->process($data, Achievement::class);
    }




    /**
     * Get all guild achievements
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     * @throws RequestException
     */
    public function getGuildAchievements(): array
    {
        $data = $this->get('achievements', 'guild/achievements');

        return $this->process($data, Achievement::class);
    }




    public function getRaces(): array
    {
        $data = $this->get('races', 'character/races');

        return $this->process($data);
    }




    public function getClasses(): array
    {
        $data = $this->get('classes', 'character/classes');

        return $this->process($data);
    }




    public function getGuildRewards()
    {
        $data = $this->get('rewards', 'guild/rewards');

        return $this->process($data);
    }




    public function getGuildPerks()
    {
        $data = $this->get('perks', 'guild/perks');

        return $this->process($data);
    }




    public function getItemClasses()
    {
        $data = $this->get('classes', 'item/classes');

        return $this->process($data);
    }




    public function getTalents()
    {
        $data = $this->get(null, 'talents');

        return $this->process($data);
    }




    public function getPetTypes()
    {
        $data = $this->get('petTypes', 'pet/types');

        return $this->process($data);
    }




    public function getBattleGroups()
    {
        $data = $this->get('battlegroups', 'battlegroups');

        return $this->process($data);
    }
}
