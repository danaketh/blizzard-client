<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Endpoint;

use danaketh\Exception\RequestException;
use danaketh\Support\Request;



/**
 * Class CharacterEndpoint
 *
 * @package danaketh\Blizzard\Game\WoW\Endpoint
 * @author  Daniel Tlach <daniel@danaketh.com>
 */
class CharacterEndpoint extends AbstractEndpoint
{
    /**
     * @var string $endpoint
     */
    protected $endpoint = '/wow/character';




    /**
     * Try to find a character
     *
     * @param string $name
     * @param string $realm
     *
     * @param array  $with Additional data to retrieve.
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function find($name, $realm, array $with = [])
    {
        $url = $this->createUrl(sprintf('%s/%s', $realm, $name), [
            'fields' => implode(',', $with)
        ]);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        return $this->process($response['body']);
    }

}
