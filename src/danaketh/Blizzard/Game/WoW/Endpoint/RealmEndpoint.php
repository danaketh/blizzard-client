<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Endpoint;

use danaketh\Blizzard\Game\WoW\Model\Realm;
use danaketh\Exception\RequestException;
use danaketh\Support\Request;



class RealmEndpoint extends AbstractEndpoint
{
    /**
     * @var string $endpoint
     */
    protected $endpoint = '/wow/realm';




    /**
     * Find and return all realms
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     * @throws RequestException
     */
    public function findAll(): array
    {
        $url = $this->createUrl('status');
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new RequestException($e->getMessage());
        }

        if (!$this->mapper) {
            return $response['body']['realms'];
        }

        $results = [];

        /** @var array $r */
        foreach ($response['body']['realms'] as $r) {
            $results[] = $this->mapper->hydrate($r, Realm::class);
        }

        return $results;
    }




    /**
     * Find specific realm(s)
     *
     * @param string|array $id
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function findById($id): array
    {
        $url = $this->createUrl('status', [
            'realms' => $id,
        ]);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            return $response['body']['realms'];
        }

        $results = [];

        /** @var array $r */
        foreach ($response['body']['realms'] as $r) {
            $results[] = $this->mapper->hydrate($r, Realm::class);
        }

        return $results;
    }




    /**
     * Find realms by faction that is currently having the upper hand
     *
     * @param $faction
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function findByFaction($faction): array
    {
        $url = $this->createUrl('status', [
            'controlling-faction' => $faction,
        ]);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            return $response['body']['realms'];
        }

        $results = [];

        /** @var array $r */
        foreach ($response['body']['realms'] as $r) {
            $results[] = $this->mapper->hydrate($r, Realm::class);
        }

        return $results;
    }




    /**
     * Find realms by their status
     *
     * @param $status
     *
     * @return array
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function findByStatus($status): array
    {
        $url = $this->createUrl('status', [
            'status' => $status,
        ]);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            return $response['body']['realms'];
        }

        $results = [];

        /** @var array $r */
        foreach ($response['body']['realms'] as $r) {
            $results[] = $this->mapper->hydrate($r, Realm::class);
        }

        return $results;
    }
}
