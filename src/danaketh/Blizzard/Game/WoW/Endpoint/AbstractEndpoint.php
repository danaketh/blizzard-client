<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Endpoint;

use danaketh\Blizzard\Common\Locale;
use danaketh\Blizzard\Exception\InvalidRegionException;
use danaketh\Blizzard\Game\WoW\Common\Region;
use danaketh\Exception\RequestException;
use danaketh\Support\Mapper;
use danaketh\Support\Request;



/**
 * Class AbstractEndpoint
 *
 * @package danaketh\Blizzard\Game\WoW\Endpoint
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
abstract class AbstractEndpoint
{
    /**
     * @var string $apiKey API key
     */
    protected $apiKey;

    /**
     * @var string $apiSecret API secret
     */
    protected $apiSecret;

    /**
     * @var string $region Selected region
     */
    protected $region;

    /**
     * @var string $locale Selected locale for response
     */
    protected $locale = Locale::English;

    /**
     * @var string $apiUrl Base API URL
     */
    protected $apiUrl = 'https://%s.api.battle.net';

    /**
     * @var Mapper $mapper
     */
    protected $mapper;




    /**
     * API constructor.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct($key, $secret)
    {
        $this->apiKey = $key;
        $this->apiSecret = $secret;
    }




    /**
     * Select region from which the API gets data
     *
     * @param string $region
     *
     * @throws InvalidRegionException
     */
    public function setRegion($region)
    {
        $regions = [
            Region::China,
            Region::Europe,
            Region::Korea,
            Region::SouthEastAsia,
            Region::Taiwan,
            Region::USA
        ];

        if (!\in_array($region, $regions, true)) {
            throw new InvalidRegionException(sprintf('Unknown region %s', $region));
        }

        $this->region = $region;
    }




    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }




    /**
     * @param Mapper $mapper
     */
    public function attachMapper(Mapper $mapper): void
    {
        $this->mapper = $mapper;
    }




    /**
     * @param string $path
     * @param array  $parameters
     *
     * @return string
     */
    protected function createUrl($path, array $parameters = []): string
    {
        $params = \array_merge($parameters, [
            'locale' => $this->locale,
            'apikey' => $this->apiKey,
        ]);

        if ($path === null || $path === '') {
            return \sprintf($this->apiUrl . '%s?%s', $this->region, $this->endpoint, \http_build_query($params));
        }

        return \sprintf($this->apiUrl . '%s/%s?%s', $this->region, $this->endpoint, $path, \http_build_query($params));
    }




    protected function get($data, $from): array
    {
        $url = $this->createUrl($from);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new RequestException($e->getMessage());
        }

        return $data === null ? $response['body'] : $response['body'][$data];
    }




    protected function process($data, $class = null): array
    {
        if (!$this->mapper || $class === null) {
            return $data;
        }

        $results = [];

        /** @var array $r */
        foreach ($data as $r) {
            $results[] = $this->mapper->hydrate($r, $class);
        }

        return $results;
    }
}
