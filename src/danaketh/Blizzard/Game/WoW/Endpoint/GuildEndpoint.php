<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Endpoint;

use danaketh\Blizzard\Game\WoW\Model\Guild;
use danaketh\Exception\RequestException;
use danaketh\Support\Request;



/**
 * Class GuildEndpoint
 *
 * @package danaketh\Blizzard\Game\WoW\Endpoint
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class GuildEndpoint extends AbstractEndpoint
{
    /**
     * @var string $endpoint
     */
    protected $endpoint = '/wow/guild';




    /**
     * Get basic guild info
     *
     * @param string $name
     * @param string $realm
     *
     * @param array  $with Additional data to retrieve.
     *
     * @return array|Guild
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function find($name, $realm, array $with = [])
    {
        $url = $this->createUrl(sprintf('%s/%s', $realm, $name), [
            'fields' => implode(',', $with)
        ]);
        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            return $response['body'];
        }

        return $this->mapper->hydrate($response['body'], Guild::class);
    }

}
