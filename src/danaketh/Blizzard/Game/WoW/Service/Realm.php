<?php

namespace danaketh\BattleNet\WoW\WoW\Service;

use danaketh\BattleNet\WoW\Abstracts\API;
use danaketh\BattleNet\WoW\WoW\Entity\Realm as RealmEntity;
use GuzzleHttp\Client;



class Realm extends API
{

    protected $prefixUrl = '%s/wow/realm';




    /**
     * Get status of all realms or a set realm
     *
     * @param string $realm
     *
     * @return array
     * @throws \HttpResponseException
     */
    public function getStatus($realm = null)
    {
        $serviceUrl = $this->getServiceUrl();
        $requestUrl = sprintf('%s/status?locale=en_GB&apikey=%s', $serviceUrl, $this->key);
        $client = new Client();
        $res = $client->request('GET', $requestUrl);

        if ($res->getStatusCode() === 200) { // on success
            $results = json_decode($res->getBody(), true);
            $realms = [];

            foreach ($results['realms'] as $r) {
                $realms[] = $this->processRealmStatus($r);
            }

            return $realms;
        } else { // on error
            throw new \HttpResponseException(sprintf(
                '[%s] %s',
                $res->getStatusCode(),
                $res->getBody()
            ));
        }
    }




    /**
     * @param array $r
     *
     * @return \danaketh\BattleNet\WoW\WoW\Entity\Realm
     */
    protected function processRealmStatus(array $r)
    {
        $realm = new RealmEntity();

        return $realm
            ->setName($r['name'])
            ->setLocale($r['locale'])
            ->setBattleGroup($r['battlegroup'])
            ->setType($r['type'])
            ->setTimezone($r['timezone'])
            ->setPopulation($r['population'])
            ->setQueue($r['queue'])
            ->setStatus($r['status'])
            ->setSlug($r['slug'])
            ->setConnectedRealms($r['connected_realms']);
    }
}
