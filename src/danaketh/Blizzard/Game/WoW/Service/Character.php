<?php

namespace danaketh\BattleNet\WoW\WoW\Service;

use danaketh\BattleNet\WoW\Abstracts\API;
use danaketh\BattleNet\WoW\WoW\Entity\Realm as RealmEntity;
use GuzzleHttp\Client;



/**
 * Character service
 *
 * @package danaketh\BattleNet\API\WoW\Service
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class Character extends API
{
    protected $prefixUrl = '%s/wow/character';

    protected $realm;

    protected $character;

    protected $fields = [];

    protected static $allFields = [
        'guild',
        'achievements',
        'appearance',
        'feed',
        'hunterPets',
        'items',
        'mounts',
        'pets',
        'petSlots',
        'professions',
        'progression',
        'pvp',
        'quests',
        'reputation',
        'statistics',
        'stats',
        'talents',
        'titles',
        'audit'
    ];


    public function setRealm($string)
    {
        $this->realm = $string;

        return $this;
    }




    public function setCharacter($string)
    {
        $this->character = $string;

        return $this;
    }




    /**
     * Get status of all realms or a set realm
     *
     * @param boolean $full Request full profile
     *
     * @return RealmEntity
     * @throws \HttpResponseException
     */
    public function get($full = false)
    {
        if ($full === true) {
            $this->fields = self::$allFields;
        }

        $serviceUrl = $this->getServiceUrl();
        $requestUrl = sprintf(
            '%s/%s/%s?locale=en_GB&fields=%s&apikey=%s',
            $serviceUrl,
            $this->realm,
            $this->character,
            implode(',', $this->fields),
            $this->key
        );
        $client = new Client();
        $res = $client->request('GET', $requestUrl);

        if ($res->getStatusCode() === 200) { // on success
            $results = json_decode($res->getBody(), true);

            return $this->map($results);
        } else { // on error
            throw new \HttpResponseException(sprintf(
                '[%s] %s',
                $res->getStatusCode(),
                $res->getBody()
            ));
        }
    }




    /**
     * Request guild information
     *
     * @return $this
     */
    public function withGuild()
    {
        $this->fieldSwitch('guild');

        return $this;
    }




    /**
     * Request achievements
     *
     * @return $this
     */
    public function withAchievements()
    {
        $this->fieldSwitch('achievements');

        return $this;
    }




    /**
     * Request appearance
     *
     * @return $this
     */
    public function withAppearance()
    {
        $this->fieldSwitch('appearance');

        return $this;
    }




    /**
     * Request activity feed
     *
     * @return $this
     */
    public function withFeed()
    {
        $this->fieldSwitch('feed');

        return $this;
    }




    /**
     * Request obtained combat pets
     *
     * @return $this
     */
    public function withHunterPets()
    {
        $this->fieldSwitch('hunterPets');

        return $this;
    }




    /**
     * Request equipped items
     *
     * @return $this
     */
    public function withItems()
    {
        $this->fieldSwitch('items');

        return $this;
    }




    /**
     * Request obtained mounts
     *
     * @return $this
     */
    public function withMounts()
    {
        $this->fieldSwitch('mounts');

        return $this;
    }




    /**
     * Request obtained battle pets
     *
     * @return $this
     */
    public function withPets()
    {
        $this->fieldSwitch('pets');

        return $this;
    }




    /**
     * Request current battle pets slots
     *
     * @return $this
     */
    public function withPetSlots()
    {
        $this->fieldSwitch('petSlots');

        return $this;
    }




    /**
     * Request professions (does not include class professions)
     *
     * @return $this
     */
    public function withProfessions()
    {
        $this->fieldSwitch('professions');

        return $this;
    }




    /**
     * Request raids and bosses progression and completeness
     *
     * @return $this
     */
    public function withProgression()
    {
        $this->fieldSwitch('progressions');

        return $this;
    }




    /**
     * Request PvP information
     *
     * @return $this
     */
    public function withPvP()
    {
        $this->fieldSwitch('pvp');

        return $this;
    }




    /**
     * Request completed quests
     *
     * @return $this
     */
    public function withQuests()
    {
        $this->fieldSwitch('quests');

        return $this;
    }




    /**
     * Request reputation with factions
     *
     * @return $this
     */
    public function withReputation()
    {
        $this->fieldSwitch('reputation');

        return $this;
    }




    /**
     * Request map of statistics. This can be quite lengthy.
     *
     * @return $this
     */
    public function withStatistics()
    {
        $this->fieldSwitch('statistics');

        return $this;
    }




    /**
     * Request attributes and stats
     *
     * @return $this
     */
    public function withStats()
    {
        $this->fieldSwitch('stats');

        return $this;
    }




    /**
     * Request talent structure
     *
     * @return $this
     */
    public function withTalents()
    {
        $this->fieldSwitch('talents');

        return $this;
    }




    /**
     * Request obtained titles
     *
     * @return $this
     */
    public function withTitles()
    {
        $this->fieldSwitch('titles');

        return $this;
    }




    /**
     * Request raw audit data
     *
     * @return $this
     */
    public function withAudit()
    {
        $this->fieldSwitch('audit');

        return $this;
    }




    /**
     * @param array $r
     *
     * @return \danaketh\BattleNet\WoW\WoW\Entity\Character
     */
    protected function map(array $r)
    {
        file_put_contents('Darkstance.json', json_encode($r));
    }




    /**
     * Switch for requested fields
     *
     * @param string $field
     */
    protected function fieldSwitch($field)
    {
        if (!in_array($field, $this->fields, true)) {
            $this->fields[] = $field;
        }
    }
}
