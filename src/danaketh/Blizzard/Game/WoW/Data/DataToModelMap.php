<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Game\WoW\Data;

use danaketh\Blizzard\Game\WoW\Model\Guild;
use danaketh\Blizzard\Game\WoW\Model\GuildNews;
use danaketh\Blizzard\Game\WoW\Model\Realm;



/**
 * Class DataToModelMap
 *
 * @package danaketh\Blizzard\Game\WoW\Data
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class DataToModelMap
{
    /**
     * @return array
     */
    public static function map(): array
    {
        return [
            Realm::class     => [
                'type',
                'population',
                'queue',
                'status',
                'name',
                'slug',
                'battle_group'     => 'battleGroup',
                'locale',
                'timezone',
                'connected_realms' => 'connectedRealms',
            ],
            Guild::class     => [
                'name',
                'realm',
                'battlegroup' => 'battleGroup',
                'level',
                'side',
                'achievementPoint',
                'lastModified',
                'emblem',
                'news'        => [
                    'name'  => 'news',
                    'class' => GuildNews::class,
                    'type'  => 'array'
                ],
                'achievements'
            ],
            GuildNews::class => [
                'type',
                'character',
                'timestamp',
                'context',
                'bonusLists',
                'achievement',
            ],

        ];
    }
}
