<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Account\Endpoint;

use danaketh\Blizzard\Account\Model\Account;
use danaketh\Blizzard\Common\Locale;
use danaketh\Blizzard\Exception\InvalidRegionException;
use danaketh\Blizzard\Game\WoW\Common\Region;
use danaketh\Exception\RequestException;
use danaketh\Support\Mapper;
use danaketh\Support\Request;



/**
 * Class UserEndpoint
 *
 * @package danaketh\Blizzard\Account\Endpoint
 * @author  Daniel Tlach <daniel@danaketh.com>
 */
class ProfileEndpoint
{
    /**
     * @var string $token OAuth2 access token
     */
    protected $token;

    /**
     * @var string $region Selected region
     */
    protected $region;

    /**
     * @var string $apiUrl Base API URL
     */
    protected $apiUrl = 'https://%s.api.battle.net';

    /**
     * @var Mapper $mapper
     */
    protected $mapper;




    /**
     * API constructor.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }




    /**
     * Select region from which the API gets data
     *
     * @param string $region
     *
     * @throws InvalidRegionException
     */
    public function setRegion($region)
    {
        $regions = [
            Region::China,
            Region::Europe,
            Region::Korea,
            Region::SouthEastAsia,
            Region::Taiwan,
            Region::USA
        ];

        if (!\in_array($region, $regions, true)) {
            throw new InvalidRegionException(sprintf('Unknown region %s', $region));
        }

        $this->region = $region;
    }




    /**
     * @param Mapper $mapper
     */
    public function attachMapper(Mapper $mapper): void
    {
        $this->mapper = $mapper;
    }




    /**
     * @param string $path
     * @param array  $parameters
     *
     * @return string
     */
    protected function createUrl($path, array $parameters = []): string
    {
        $params = \array_merge($parameters, [
            'access_token' => $this->token,
        ]);

        return \sprintf($this->apiUrl . '/%s?%s', $this->region, $path, \http_build_query($params));
    }




    /**
     * @return array|array[]|mixed|null
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function getAccount()
    {
        $url = $this->createUrl('account/user');

        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            return $response['body'];
        }

        return $this->mapper->hydrate($response['body'], Account::class);
    }




    /**
     * @return \ArrayIterator
     * @throws \danaketh\Exception\MissingMappingException
     */
    public function getWowCharacters()
    {
        $url = $this->createUrl('wow/user/characters');
        $collection = new \ArrayIterator();

        try {
            /** @var array[][] $response */
            $response = Request::get($url);
        } catch (RequestException $e) {
            throw new $e;
        }

        if (!$this->mapper) {
            foreach ($response['body']['characters'] as $char) {
                $collection->append($char);
            }

            return $collection;
        }

        foreach ($response['body']['characters'] as $char) {
            $collection->append(
                $this->mapper->hydrate($char, Account::class)
            );
        }

        return $collection;
    }
}
