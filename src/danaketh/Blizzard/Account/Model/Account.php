<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Account\Model;

/**
 * Class Account
 *
 * @package danaketh\Blizzard\Account\Model
 * @author  Daniel Tlach <daniel@danaketh.com>
 */
class Account
{
    protected $id;

    protected $battleTag;




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }




    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }




    /**
     * @return mixed
     */
    public function getBattleTag()
    {
        return $this->battleTag;
    }




    /**
     * @param mixed $battleTag
     */
    public function setBattleTag($battleTag): void
    {
        $this->battleTag = $battleTag;
    }


}
