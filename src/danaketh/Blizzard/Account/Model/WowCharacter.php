<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Blizzard\Account\Model;

/**
 * Class WowCharacter
 *
 * @package danaketh\Blizzard\Account\Model
 * @author  Daniel Tlach <daniel@danaketh.com>
 */
class WowCharacter
{
    protected $name;

    protected $realm;

    protected $battleGroup;

    protected $class;

    protected $race;

    protected $gender;

    protected $level;

    protected $achievementPoints;

    protected $thumbnail;

    protected $lastModified;

    protected $guild;

    protected $guildRealm;

    protected $specs;




    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }




    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }




    /**
     * @return mixed
     */
    public function getRealm()
    {
        return $this->realm;
    }




    /**
     * @param mixed $realm
     */
    public function setRealm($realm): void
    {
        $this->realm = $realm;
    }




    /**
     * @return mixed
     */
    public function getBattleGroup()
    {
        return $this->battleGroup;
    }




    /**
     * @param mixed $battleGroup
     */
    public function setBattleGroup($battleGroup): void
    {
        $this->battleGroup = $battleGroup;
    }




    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }




    /**
     * @param mixed $class
     */
    public function setClass($class): void
    {
        $this->class = $class;
    }




    /**
     * @return mixed
     */
    public function getRace()
    {
        return $this->race;
    }




    /**
     * @param mixed $race
     */
    public function setRace($race): void
    {
        $this->race = $race;
    }




    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }




    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }




    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }




    /**
     * @param mixed $level
     */
    public function setLevel($level): void
    {
        $this->level = $level;
    }




    /**
     * @return mixed
     */
    public function getAchievementPoints()
    {
        return $this->achievementPoints;
    }




    /**
     * @param mixed $achievementPoints
     */
    public function setAchievementPoints($achievementPoints): void
    {
        $this->achievementPoints = $achievementPoints;
    }




    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }




    /**
     * @param mixed $thumbnail
     */
    public function setThumbnail($thumbnail): void
    {
        $this->thumbnail = $thumbnail;
    }




    /**
     * @return mixed
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }




    /**
     * @param mixed $lastModified
     */
    public function setLastModified($lastModified): void
    {
        $this->lastModified = $lastModified;
    }




    /**
     * @return mixed
     */
    public function getGuild()
    {
        return $this->guild;
    }




    /**
     * @param mixed $guild
     */
    public function setGuild($guild): void
    {
        $this->guild = $guild;
    }




    /**
     * @return mixed
     */
    public function getGuildRealm()
    {
        return $this->guildRealm;
    }




    /**
     * @param mixed $guildRealm
     */
    public function setGuildRealm($guildRealm): void
    {
        $this->guildRealm = $guildRealm;
    }




    /**
     * @return mixed
     */
    public function getSpecs()
    {
        return $this->specs;
    }




    /**
     * @param mixed $specs
     */
    public function setSpecs($specs): void
    {
        $this->specs = $specs;
    }


}
