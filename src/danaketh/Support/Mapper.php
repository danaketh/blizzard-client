<?php

/*
 * This file is part of the WoW API.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Support;

use danaketh\Exception\MissingMappingException;



/**
 * Class Mapper
 *
 * Maps data from an array onto an object (entity, model, whatever you like to call it)
 *
 * @package danaketh\Support
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class Mapper
{
    /** @var array Map describing class meta */
    protected $map;




    /**
     * @param array $map Map describing class meta
     */
    public function __construct(array $map = array())
    {
        $this->map = $map;
    }




    /**
     * Hydrate from array to object
     *
     * @param array  $data
     * @param string $className
     *
     * @return array|mixed|null
     * @throws MissingMappingException
     */
    public function hydrate(array $data, $className)
    {
        $class = new $className;

        if (!isset($this->map[$className])) {
            throw new MissingMappingException(sprintf('Unable to find mapping for %s', $className));
        }

        $map = $this->buildMap($this->map[$className]);

        foreach ($data as $key => $value) {
            if (!isset($map[$key])) {
                continue;
            }

            $pointer = $map[$key];
            $setter = $this->getSetterName($pointer, $key);

            if (\is_array($pointer)) {
                if ($pointer['type'] === 'array') {
                    $value = $this->hydrateArray($value, $pointer['class']);
                } else {
                    $value = $this->hydrate($value, $pointer['class']);
                }
            }

            $class->$setter($value);
        }

        return $class;
    }




    /**
     * @param array $data
     * @param       $className
     *
     * @return array
     * @throws MissingMappingException
     */
    public function hydrateArray(array $data, $className): array
    {
        $arr = [];
        foreach ($data as $rec) {
            $arr[] = $this->hydrate($rec, $className);
        }

        return $arr;
    }




    protected function buildMap($map): array
    {
        $keys = array_keys($map);
        $values = array_values($map);
        $builtMap = [];

        /**
         * @var integer        $i
         * @var integer|string $v
         */
        foreach ($values as $i => $v) {
            $key = \is_int($keys[$i]) ? $v : $keys[$i];
            $builtMap[$key] = $v;
        }

        return $builtMap;
    }




    /**
     * @param $source
     * @param $key
     *
     * @return string
     * @throws MissingMappingException
     */
    protected function getSetterName($source, $key): string
    {
        if (!\is_array($source)) {
            return sprintf('set%s', \ucfirst($source));
        }
        if (!isset($source['name'])) {
            throw new MissingMappingException(sprintf('No mapping defined for key %s', $key));
        }

        return sprintf('set%s', \ucfirst($source['name']));
    }
}
