<?php

/*
 * This file is part of the HubSpot API Client.
 *
 * (c) danaketh, s.r.o. <dev@danaketh.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace danaketh\Exception;

/**
 * Class RequestException
 *
 * @package danaketh\Exception
 * @author  Daniel Tlach <daniel@tlach.cz>
 */
class RequestException extends \Exception
{

}
